let showTabs;
showTabs= document.querySelectorAll('.tabs-title');

showTabs.forEach(function(item){
    item.addEventListener('click', function(){
        let activeTab = document.querySelector(`.tab-content[data-tab-content="${item.dataset.tab}"]`);
        document.querySelector('.tab-content.content-active').classList.remove('content-active');
        document.querySelector('.tabs-title.active').classList.remove('active');

        activeTab.classList.add('content-active');
        item.classList.add('active');
    });
});
